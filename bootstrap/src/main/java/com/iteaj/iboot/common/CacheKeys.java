package com.iteaj.iboot.common;

public interface CacheKeys {

    String IBOOT_CACHE_ORG = "IBOOT:ORG";
    String IBOOT_CACHE_DICT = "IBOOT:DICT";
}
