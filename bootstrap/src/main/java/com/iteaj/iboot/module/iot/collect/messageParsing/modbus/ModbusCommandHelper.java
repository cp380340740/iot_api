package com.iteaj.iboot.module.iot.collect.messageParsing.modbus;

import com.iteaj.iboot.module.iot.collect.messageParsing.analyticRule;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

public class ModbusCommandHelper {

        //region Create

    // 创建功能码03的Modbus命令
    public static byte[] createFunction03(int deviceStation, int commStartAddress, int registersCount) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x03); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        addShortToCommand(command, (short) registersCount); // 寄存器数
        return convertListToByteArray(command);
    }

    // 创建功能码01的Modbus命令
    public static byte[] createFunction01(int deviceStation, int commStartAddress, int registersCount) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x01); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        addShortToCommand(command, (short) registersCount); // 寄存器数
        return convertListToByteArray(command);
    }

    // 创建功能码02的Modbus命令
    public static byte[] createFunction02(int deviceStation, int commStartAddress, int registersCount) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x02); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        addShortToCommand(command, (short) registersCount); // 寄存器数
        return convertListToByteArray(command);
    }

    // 创建功能码04的Modbus命令
    public static byte[] createFunction04(int deviceStation, int commStartAddress, int registersCount) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x04); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        addShortToCommand(command, (short) registersCount); // 寄存器数
        return convertListToByteArray(command);
    }

    // 创建功能码05的Modbus命令
    public static byte[] createFunction05(int deviceStation, int commStartAddress, boolean onOrOff) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x05); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        command.addAll(bytesOf(new byte[]{(byte) (onOrOff ? 0xff : 0x00), 0x00})); // 开/关状态
        return convertListToByteArray(command);
    }

    // 创建功能码06的Modbus命令
    public static byte[] createFunction06(int deviceStation, int commStartAddress, short value) {
        ArrayList<Byte> command = new ArrayList<>();
        command.add((byte) deviceStation);
        command.add((byte) 0x06); // 功能码
        addShortToCommand(command, (short) commStartAddress); // 开始地址
        addShortToCommand(command, value); // 写入值
        return convertListToByteArray(command);
    }

    // 辅助方法：将short值添加到命令中
    public static void addShortToCommand(ArrayList<Byte> command, short value) {
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putShort(value);
        for (byte b : buffer.array()) {
            command.add(b);
        }
    }

    // 辅助方法：将字节数组添加到命令中
    private static ArrayList<Byte> bytesOf(byte[] byteArray) {
        ArrayList<Byte> byteList = new ArrayList<>();
        for (byte b : byteArray) {
            byteList.add(b);
        }
        return byteList;
    }

    // 辅助方法：将字节列表转换为字节数组
    private static byte[] convertListToByteArray(ArrayList<Byte> byteList) {
        byte[] byteArray = new byte[byteList.size()];
        for (int i = 0; i < byteList.size(); i++) {
            byteArray[i] = byteList.get(i);
        }
        return byteArray;
    }

        //region Get
    // 读取功能码01的数据
    public static Object getFunction01(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x02) {
            return null;
        }
        if (data[2] * 8 - 1 < index) {
            return null;
        }
        int location = index / 8;
        return getBit(data[location + 2], index - location * 8);
    }

    // 读取功能码02的数据
    public static Object getFunction02(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x02) {
            return null;
        }
        if (data[2] * 8 - 1 < index) {
            return null;
        }
        int location = index / 8;
        return getBit(data[location + 2], index - location * 8);
    }

    // 读取功能码03的AB格式数据
    public static Object getFunction03_AB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort();
    }

    // 读取功能码03的BA格式数据
    public static Object getFunction03_BA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
    }

    // 读取功能码03的DCBA格式数据
    public static Object getFunction03_DCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
    }

    // 读取功能码03的ABCD格式数据
    public static Object getFunction03_ABCD(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getInt();
    }

    // 读取功能码03的浮点数ABCD格式数据
    public static Object getFunction03_F_ABCD(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        int a1 = ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        int a2 = ByteBuffer.wrap(Arrays.copyOfRange(data, 7 + index * 4, 9 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        return getFloat(a1, a2);
    }

    // 读取功能码03的浮点数CDAB格式数据
    public static Object getFunction03_F_CDAB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != (byte) deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        int a1 = ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 5 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        int a2 = ByteBuffer.wrap(Arrays.copyOfRange(data, 5 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        return getFloat(a2, a1);
    }

    // 用于提取指定位的辅助方法
    private static boolean getBit(byte b, int position) {
        return ((b >> position) & 1) == 1;
    }

    // 用于组合两个ushort值为float的辅助方法
//    private static float getFloat(int a1, int a2) {
//        int combined = (a1 << 16) | a2;
//        return Float.intBitsToFloat(combined);
//    }

    // 读取功能码03的HGFEDCBA格式数据
    public static Object getFunction03_HGFEDCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getLong();
    }

    // 读取功能码03的ABCDEFGH格式数据
    public static Object getFunction03_ABCDEFGH(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.BIG_ENDIAN).getLong();
    }

    // 读取功能码03的无符号AB格式数据
    public static Object getFunction03_U_AB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
    }

    // 读取功能码03的无符号BA格式数据
    public static Object getFunction03_U_BA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort() & 0xffff;
    }

    // 读取功能码03的无符号DCBA格式数据
    public static Object getFunction03_U_DCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt() & 0xffffffffL;
    }

    // 读取功能码03的无符号ABCD格式数据
    public static Object getFunction03_U_ABCD(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getInt() & 0xffffffffL;
    }

    // 读取功能码03的无符号HGFEDCBA格式数据
    public static Object getFunction03_U_HGFEDCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getLong() & 0xffffffffffffffffL;
    }

    // 读取功能码03的无符号ABCDEFGH格式数据
    public static Object getFunction03_U_ABCDEFGH(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x03) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.BIG_ENDIAN).getLong() & 0xffffffffffffffffL;
    }

    // 读取功能码04的AB格式数据
    public static Object getFunction04_AB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort();
    }

    // 读取功能码04的BA格式数据
    public static Object getFunction04_BA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort();
    }

    // 读取功能码04的DCBA格式数据
    public static Object getFunction04_DCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
    }


    // 读取功能码04的浮点数ABCD格式数据
    public static Object getFunction04_F_ABCD(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        int a1 = ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 5 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        int a2 = ByteBuffer.wrap(Arrays.copyOfRange(data, 5 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        return getFloat(a1, a2);
    }

    // 读取功能码04的浮点数CDAB格式数据
    public static Object getFunction04_F_CDAB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        int a1 = ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 5 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        int a2 = ByteBuffer.wrap(Arrays.copyOfRange(data, 5 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
        return getFloat(a2, a1);
    }

    // 读取功能码04的HGFEDCBA格式数据
    public static Object getFunction04_HGFEDCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getLong();
    }

    // 读取功能码04的ABCDEFGH格式数据
    public static Object getFunction04_ABCDEFGH(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.BIG_ENDIAN).getLong();
    }

    // 读取功能码04的无符号AB格式数据
    public static Object getFunction04_U_AB(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.BIG_ENDIAN).getShort() & 0xffff;
    }

    // 读取功能码04的无符号BA格式数据
    public static Object getFunction04_U_BA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 2, 5 + index * 2)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getShort() & 0xffff;
    }

    // 读取功能码04的无符号DCBA格式数据
    public static Object getFunction04_U_DCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt() & 0xffffffffL;
    }

    // 读取功能码04的无符号ABCD格式数据
    public static Object getFunction04_U_ABCD(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 4, 7 + index * 4)).order(java.nio.ByteOrder.BIG_ENDIAN).getInt() & 0xffffffffL;
    }

    // 读取功能码04的无符号HGFEDCBA格式数据
    public static Object getFunction04_U_HGFEDCBA(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.LITTLE_ENDIAN).getLong() & 0xffffffffffffffffL;
    }

    // 读取功能码04的无符号ABCDEFGH格式数据
    public static Object getFunction04_U_ABCDEFGH(int deviceStation, int index, byte[] data) {
        if (data.length < 3) {
            return null;
        }
        if (data[0] != deviceStation || data[1] != 0x04) {
            return null;
        }
        if (data[2] < index) {
            return null;
        }
        return ByteBuffer.wrap(Arrays.copyOfRange(data, 3 + index * 8, 11 + index * 8)).order(java.nio.ByteOrder.BIG_ENDIAN).getLong() & 0xffffffffffffffffL;
    }

    // 将两个ushort值合并为浮点数
    private static float getFloat(int p1, int p2) {
        int sign = p1 / 32768;
        int rest = p1 % 32768;
        int exponent = rest / 128;
        int mantissa = rest % 128;
        float digit = (mantissa * 65536f + p2) / 8388608f;
        return (float) Math.pow(-1, sign) * (float) Math.pow(2, exponent - 127) * (1 + digit);
    }

    // 检查消息ID是否匹配
    public static boolean checkMessageID(byte[] data, short messageId) {
        if (data.length < 8) {
            return false;
        }
        short dataMessageID = ByteBuffer.wrap(new byte[]{data[0], data[1]}).order(java.nio.ByteOrder.BIG_ENDIAN).getShort();
        return dataMessageID == messageId;
    }
    //验证CRC
    private static boolean validateCRC(String msg, int offset, int length, String crcValue) {
        int calculatedCRC = calculateCRC(msg, offset, length);
        // 解析 crcValue 字符串为整数
        int parsedCRC = Integer.parseInt(crcValue, 16);
        return (calculatedCRC & 0xFFFF) == (parsedCRC & 0xFFFF);
    }


    private static int calculateCRC(String msg, int offset, int length) {
        int crc = 0xFFFF;
        for (int i = offset; i < offset + length; i++) {
            crc ^= msg.charAt(i) & 0xFF;
            for (int j = 0; j < 8; j++) {
                if ((crc & 0x0001) != 0) {
                    crc = (crc >> 1) ^ 0xA001;
                } else {
                    crc = crc >> 1;
                }
            }
        }
        return crc;
    }

    public static Object analyticCommFunction3(analyticRule.FliterType fliterType, int deviceStation, int registersIndex, byte[] msg) {
        switch (fliterType){
            case AB:
                return getFunction03_AB(deviceStation,registersIndex,msg);
            case BA:
                return getFunction03_BA(deviceStation,registersIndex,msg);
            case ABCD:
                return getFunction03_ABCD(deviceStation,registersIndex,msg);
            case U_AB:
                return getFunction03_U_AB(deviceStation,registersIndex,msg);
            case U_BA:
                return getFunction03_U_BA(deviceStation,registersIndex,msg);
            case U_ABCD:
                return getFunction03_U_ABCD(deviceStation,registersIndex,msg);
            case U_ABCDEFGH:
                return getFunction03_U_ABCDEFGH(deviceStation,registersIndex,msg);
            case ABCDEFGH:
                return getFunction03_ABCDEFGH(deviceStation,registersIndex,msg);
            case HGFEDCBA:
                return getFunction03_HGFEDCBA(deviceStation,registersIndex,msg);
            case DCBA:
                return getFunction03_DCBA(deviceStation,registersIndex,msg);
            case U_DCBA:
                return getFunction03_U_DCBA(deviceStation,registersIndex,msg);
            case U_HGFEDCBA:
                return getFunction03_U_HGFEDCBA(deviceStation,registersIndex,msg);
            case F_CDAB:
                return getFunction03_F_CDAB(deviceStation,registersIndex,msg);
            case F_ABCD:
                return getFunction03_F_ABCD(deviceStation,registersIndex,msg);
            case Wrong:
                return null;
            default:
                return null;
        }
    }

    public static Object analyticCommFunction4(analyticRule.FliterType fliterType, int deviceStation, int registersIndex, byte[] msg) {
        switch (fliterType){
            case AB:
                return getFunction04_AB(deviceStation,registersIndex,msg);
            case BA:
                return getFunction04_BA(deviceStation,registersIndex,msg);
//            case ABCD:
//                return getFunction04_ABCD(deviceStation,registersIndex,msg);
            case U_AB:
                return getFunction04_U_AB(deviceStation,registersIndex,msg);
            case U_BA:
                return getFunction04_U_BA(deviceStation,registersIndex,msg);
            case U_ABCD:
                return getFunction04_U_ABCD(deviceStation,registersIndex,msg);
            case U_ABCDEFGH:
                return getFunction04_U_ABCDEFGH(deviceStation,registersIndex,msg);
            case ABCDEFGH:
                return getFunction04_ABCDEFGH(deviceStation,registersIndex,msg);
            case HGFEDCBA:
                return getFunction04_HGFEDCBA(deviceStation,registersIndex,msg);
            case DCBA:
                return getFunction04_DCBA(deviceStation,registersIndex,msg);
            case U_DCBA:
                return getFunction04_U_DCBA(deviceStation,registersIndex,msg);
            case U_HGFEDCBA:
                return getFunction04_U_HGFEDCBA(deviceStation,registersIndex,msg);
            case F_CDAB:
                return getFunction04_F_CDAB(deviceStation,registersIndex,msg);
            case F_ABCD:
                return getFunction04_F_ABCD(deviceStation,registersIndex,msg);
            case Wrong:
                return null;
            default:
                return null;
        }
    }
}