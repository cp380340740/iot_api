package com.iteaj.iboot.module.iot.collect.messageParsing;

import com.baomidou.mybatisplus.annotation.TableId;
import com.iteaj.framework.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 对应设备报文解析规则
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class messageAnalytic extends BaseEntity {

    @TableId
    private Long analyticId;

    /**
     * 网关解析规则
     */
    private Long RuleId;

    /**
     * 解析规则名称
     */
    private String analyticName;

    /**
     * 寄存器索引地址
     */
    private String registerAddressIndex;

    /**
     *基数
     */
    private int cardinalNumber;

    /**
     * 倍数
     */
    private int multiple;
}
