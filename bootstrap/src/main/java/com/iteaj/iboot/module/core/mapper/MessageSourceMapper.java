package com.iteaj.iboot.module.core.mapper;

import com.iteaj.iboot.module.core.entity.MessageSource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息源 Mapper 接口
 * </p>
 *
 * @author iteaj
 * @since 2023-07-30
 */
public interface MessageSourceMapper extends BaseMapper<MessageSource> {

}
