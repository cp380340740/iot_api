package com.iteaj.iboot.module.iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iteaj.framework.result.Result;
import com.iteaj.iboot.module.iot.collect.messageParsing.analyticRule;
import com.iteaj.iboot.module.iot.entity.Signal;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AnalyticRuleMapper extends BaseMapper<analyticRule> {

    @Select("SELECT * from iot_analytic_rule where rule_id=#{ruleId}")
    public analyticRule getById(@Param("ruleId")Long ruleId);

    @Select("select * from iot_analytic_rule where analytic_rule=#{name}")
    public analyticRule getByName(@Param("name")String name);

    @Select("select * from iot_analytic_rule")
    Result<Signal> getAll();
}
