package com.iteaj.iboot.module.iot.collect.messageParsing;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.iteaj.framework.BaseEntity;
import lombok.Data;

/**
 * 网关解析规则配置
 */

@Data
@TableName("iot_analytic_rule")
public class analyticRule extends BaseEntity {

    private Long ruleId;

    /**
     * 解析规则名称(用户自定义)
     */
    private String analyticRule;

    /**
     * 站号
     */
    private int deviceStation;

    /**
     * 功能码
     */
    private int commFunction;

    /**
     * FilterNumber->Fliter
     */
    private int filterNumber;

    /**
     * 寄存器起始地址
     */
    private int registersAddress;

    /**
     * 寄存器数量
     */
    private int registersCount;

    /**
     * 数据类型：abcd/bcda.....
     */
    @TableField(exist = false)
    private FliterType fliter;


    /*    public enum CommFliterType {
            Send(0,"Send"),
            Receive(1,"Receive");

            int code;
            String type;
            CommFliterType(int code,String type){
                this.type=type;
                this.code=code;
            }
            public int getCode(){
                return code;
            }
            public String getType(){
                return type;
            }
        }*/
    public enum FliterType {
        ABCD(0, "ABCD"),
        DCBA(1, " DCBA"),
        AB(2, "AB"),
        BA(3, "BA"),
        A(4, "A"),
        ABCDEFGH(5, "ABCDEFGH"),
        HGFEDCBA(6, "HGFEDCBA"),
        U_ABCD(7, "U_ABCD"),
        U_DCBA(8, "U_DCBA"),
        U_AB(9, "U_AB"),
        U_BA(10, "U_BA"),
        U_ABCDEFGH(11, " U_ABCDEFGH"),
        U_HGFEDCBA(12, "U_HGFEDCBA"),
        F_ABCD(13, "F_ABCD"),
        F_CDAB(14, " F_CDAB "),
        Wrong(15, "wrongType");

        String type;
        int code;

        FliterType(int code, String type) {
            this.code = code;
            this.type = type;
        }

        public int getCode() {
            return code;
        }

        public String getType() {
            return type;
        }

    }

    public FliterType getType(int code) {
        switch (code) {
            case 0:
                return FliterType.ABCD;
            case 1:
                return FliterType.DCBA;
            case 2:
                return FliterType.AB;
            case 3:
                return FliterType.BA;
            case 4:
                return FliterType.A;
            case 5:
                return FliterType.ABCDEFGH;
            case 6:
                return FliterType.HGFEDCBA;
            case 7:
                return FliterType.U_ABCD;
            case 8:
                return FliterType.U_DCBA;
            case 9:
                return FliterType.U_AB;
            case 10:
                return FliterType.U_BA;
            case 11:
                return FliterType.U_ABCDEFGH;
            case 12:
                return FliterType.U_HGFEDCBA;
            case 13:
                return FliterType.F_ABCD;
            case 14:
                return FliterType.F_CDAB;
            default:
                return FliterType.Wrong;
        }
    }

    public analyticRule(String analyticRule, int deviceStation,
                        int commFunction, int filterNumber, int registersAddress,
                        int registersCount) {
        this.analyticRule = analyticRule;
        this.deviceStation = deviceStation;
        this.commFunction = commFunction;
        this.filterNumber = filterNumber;
        this.registersAddress = registersAddress;
        this.registersCount = registersCount;
        this.fliter = getType(filterNumber);
    }

    public Object getParser(analyticRule analyticRule) {
//        int commFunction = analyticRule.getCommFunction();
//        int number = analyticRule.getFilterNumber();
//        FliterType type = getType(number);
//        switch (commFunction) {
//
//        }
        return null;
    }
}