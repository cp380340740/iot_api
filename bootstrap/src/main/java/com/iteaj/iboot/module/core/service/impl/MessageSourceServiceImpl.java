package com.iteaj.iboot.module.core.service.impl;

import com.iteaj.iboot.module.core.entity.MessageSource;
import com.iteaj.iboot.module.core.mapper.MessageSourceMapper;
import com.iteaj.iboot.module.core.service.IMessageSourceService;
import com.iteaj.framework.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息源 服务实现类
 * </p>
 *
 * @author iteaj
 * @since 2023-07-30
 */
@Service
public class MessageSourceServiceImpl extends BaseServiceImpl<MessageSourceMapper, MessageSource> implements IMessageSourceService {

}
