package com.iteaj.iboot.module.iot.service;

import com.iteaj.framework.IBaseService;
import com.iteaj.iboot.module.iot.collect.messageParsing.analyticRule;

public interface IAnalyticRuleService  extends IBaseService<analyticRule> {
}
