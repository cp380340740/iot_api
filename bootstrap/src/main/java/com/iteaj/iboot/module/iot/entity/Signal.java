package com.iteaj.iboot.module.iot.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.iteaj.framework.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.iteaj.iboot.module.iot.collect.messageParsing.analyticRule;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 寄存器点位
 * </p>
 *
 * @author iteaj
 * @since 2022-07-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("iot_signal")
public class  Signal extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 点位名称
     */
    private String name;


    /**
     * 设备类型
     */
    @TableField(exist = false)
    private Long typeId;

    /**
     * 信号类型(1. 点位 2. 自定义报文 )
     */
    private Integer type;
    /**
     * 报文编码(HEX, UTF8, ASCII)
     */
    private String encode;

    /**
     * 字段类型
     */
    private int fieldType;

    /**
     * 自定义报文
     */
    private String message;



    /**
     * 型号id
     */
    private Integer modelId;

    /**
     * 点位字段名称
     */
    private String fieldName;


    private Date createTime;

    private Date updateTime;



//    /**
//     * 解析规则名称(用户自定义)
//     */
//    @TableField(exist = false)
//    private String AnalyticRule;

    /**
     *站号
     */

    private int DeviceStation ;

    /**
     * 功能码
     */

    private int CommFunction ;
    /**
     * 数据类型
     */
    private int typeOptions;

    /**
     * 寄存器起始地址
     */

    private String RegistersAddress  ;

    /**
     *寄存器数量
     */
    private int RegistersCount ;

    /**
     * 寄存器起始索引
     */
    private int RegistersIndex;

    /**
     * 经度
     */
    private int Longitude;

    /**
     * 纬度
     */
    private int Dimension;

    /**
     * 计量单位
     */
    private String CountUnit;

    /**
     * 绝对数
     */
    private boolean Absolute;

    /**
     * 系数
     */
    private boolean Coefficient;

    /**
     * 基数
     */
    private int BaseNumber;

    /**
     * 倍数
     */
    private int Multiple;

    /**
     * 超时时间
     */
    private long overTime;

    @TableField(exist = false)
    private FliterType fliter;

    public enum FliterType {
        ABCD(0, "ABCD"),
        DCBA(1, " DCBA"),
        AB(2, "AB"),
        BA(3, "BA"),
        A(4, "A"),
        ABCDEFGH(5, "ABCDEFGH"),
        HGFEDCBA(6, "HGFEDCBA"),
        U_ABCD(7, "U_ABCD"),
        U_DCBA(8, "U_DCBA"),
        U_AB(9, "U_AB"),
        U_BA(10, "U_BA"),
        U_ABCDEFGH(11, " U_ABCDEFGH"),
        U_HGFEDCBA(12, "U_HGFEDCBA"),
        F_ABCD(13, "F_ABCD"),
        F_CDAB(14, " F_CDAB "),
        Wrong(15, "wrongType");

        String type;
        int code;

        FliterType(int code, String type) {
            this.code = code;
            this.type = type;
        }

        public int getCode() {
            return code;
        }

        public String getType() {
            return type;
        }

    }

    public analyticRule.FliterType getType(int code) {
        switch (code) {
            case 0:
                return analyticRule.FliterType.ABCD;
            case 1:
                return analyticRule.FliterType.DCBA;
            case 2:
                return analyticRule.FliterType.AB;
            case 3:
                return analyticRule.FliterType.BA;
            case 4:
                return analyticRule.FliterType.A;
            case 5:
                return analyticRule.FliterType.ABCDEFGH;
            case 6:
                return analyticRule.FliterType.HGFEDCBA;
            case 7:
                return analyticRule.FliterType.U_ABCD;
            case 8:
                return analyticRule.FliterType.U_DCBA;
            case 9:
                return analyticRule.FliterType.U_AB;
            case 10:
                return analyticRule.FliterType.U_BA;
            case 11:
                return analyticRule.FliterType.U_ABCDEFGH;
            case 12:
                return analyticRule.FliterType.U_HGFEDCBA;
            case 13:
                return analyticRule.FliterType.F_ABCD;
            case 14:
                return analyticRule.FliterType.F_CDAB;
            default:
                return analyticRule.FliterType.Wrong;
        }
    }

    public Signal() { }

    public Signal(Integer modelId) {
        this.modelId = modelId;
    }
}
