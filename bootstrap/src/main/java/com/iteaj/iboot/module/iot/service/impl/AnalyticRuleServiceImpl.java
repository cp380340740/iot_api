package com.iteaj.iboot.module.iot.service.impl;

import com.iteaj.framework.BaseServiceImpl;
import com.iteaj.iboot.module.iot.collect.messageParsing.analyticRule;
import com.iteaj.iboot.module.iot.mapper.AnalyticRuleMapper;
import com.iteaj.iboot.module.iot.service.IAnalyticRuleService;
import org.springframework.stereotype.Service;

@Service
public class AnalyticRuleServiceImpl extends BaseServiceImpl<AnalyticRuleMapper, analyticRule> implements IAnalyticRuleService {
}
